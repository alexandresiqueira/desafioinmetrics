Este projeto foi desenvolvido utilizando JAVA, Cucumber, Maven e as bibliotecas Google Json

Basta clonar a pasta e abrir no Elipse;

Para WebDriver  src > test > java > StepDefinitions > RunCukeTests.java e Run as Junit Test
Para API na pasta src > test > java > API > ApiFilmsTest.java e run as Junit Test 

Para rodar por linha de comando: mvn clean test

Pode ser necessário ajustar a versão do java