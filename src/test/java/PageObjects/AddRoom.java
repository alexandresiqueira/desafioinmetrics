package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AddRoom {

	public static WebDriverWait waitVar;

	private WebDriver driver;

	public AddRoom(WebDriver d) {
		driver = d;
	}

	public void clickTabGeneral() {
		driver.findElement(By.linkText("General")).click();
	}

	public void clickTabMAmenities() {
		driver.findElement(By.linkText("Amenities")).click();
	}

	public void selectStatusGeneral() {
		Select selectByValue = new Select(driver.findElement(By.name("roomstatus")));
		selectByValue.selectByValue("No");

	}

	public void selectRoomTypeGeneral() throws InterruptedException {
		/*
		 * Select selectByValue = new Select(driver.findElement(By.name("roomstatus")));
		 * selectByValue.selectByValue("No");
		 */
		driver.findElement(By.id("s2id_autogen1")).click();
		driver.findElement(By.xpath("//*[@id='select2-drop']/div/input")).sendKeys("Triple");
		driver.findElement(By.xpath("//*[@id='select2-drop']/ul")).click();
		Thread.sleep(1000);
	}

	public void selectHotelGeneral() throws InterruptedException {
		/*
		 * Select selectByValue = new Select(driver.findElement(By.name("roomstatus")));
		 * selectByValue.selectByValue("No");
		 */
		driver.findElement(By.id("s2id_autogen3")).click();
		driver.findElement(By.xpath("//*[@id='select2-drop']/div/input")).sendKeys("monks");
		driver.findElement(By.xpath("//*[@id='select2-drop']/ul")).click();
		Thread.sleep(1000);
	}

	public void insertPriceGeneral() {
		driver.findElement(By.name("basicprice")).sendKeys("300");
	}

	public void insertQuantityGeneral() {
		driver.findElement(By.name("quantity")).sendKeys("3");
	}

	public void insertMinimumStayGeneral() {
		driver.findElement(By.name("minstay")).sendKeys("5");
	}

	public void insertMaxAdultsGeneral() {
		driver.findElement(By.name("adults")).sendKeys("2");
	}

	public void insertMaxChildrenGeneral() {
		driver.findElement(By.name("children")).sendKeys("1");
	}

	public void insertBedExtraGeneral() {
		driver.findElement(By.name("extrabeds")).sendKeys("1");
	}

	public void insertExtraBedChergesGeneral() {
		driver.findElement(By.name("bedcharges")).sendKeys("80");
	}

	public void checkAmenitiesAmenitie() {
		int selects = BaseObject.getRandomNumber(5);
		for (int i = 1; i <= selects; i++) {
			int posicao = BaseObject.getRandomNumber(20);
			if (posicao > 3 && posicao < 20) {
				driver.findElement(By.xpath("//*[@id='AMENITIES']/div/div/div[" + posicao + "]/label/div/ins")).click();
			}
		}

	}

	public void clickSubmitAddRoom() {
		driver.findElement(By.id("add")).click();
	}

}