package PageObjects;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class BaseObject {
	// static ChromeOptions options = new ChromeOptions();
	public static WebDriver driver;
	public static String URL = "https://phptravels.net/supplier";
	private static Random random = new Random();

	public static WebDriver startPage() {
		// System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")
		// + "\\drivers\\chromedriver-v2-46.exe");
		ChromeOptions options = new ChromeOptions();
		options.setAcceptInsecureCerts(true);
		// options.setUnhandledPromptBehaviour(UnexpectedAlertBehaviour.ACCEPT);
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		driver.get(URL);
		return driver;
	}

	public static WebDriver getDriver() {
		return driver;
	}

	public static void stop() {
		driver.close();

	}

	public static String getRandomBoolean(String verdadeiro, String falso) {
		Boolean retorno = random.nextBoolean();
		if (retorno) {
			return verdadeiro;
		} else {
			return falso;
		}
	}
	
	public static int getRandomNumber(int limit) {
        Random rand = new Random();
        int numero = rand.nextInt(limit);
        return numero;
    }
    

}
