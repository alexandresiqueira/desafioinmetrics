package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Home{

	public static WebDriverWait waitVar;

	private WebDriver driver; 
	
	
	public Home(WebDriver d) {
		driver = d;
	}
	
	/****************************** home ************************/
	
	public void clickMenuHotels() {
		driver.findElement(By.linkText("HOTELS")).click();
	}
	
	public void clickSubMenuAddRoom() {
		driver.findElement(By.linkText("ADD ROOM")).click();
	}

	public void clickSubMenuManageRoom() {
		driver.findElement(By.linkText("MANAGE ROOMS")).click();
	}
	
	public void clickTabGeneral() {
		driver.findElement(By.linkText("General")).click();
	}
	
	public void clickTabMAmenities() {
		driver.findElement(By.linkText("Amenities")).click();
	}
	
	public void selectStatusGeneral() {
		Select selectByValue = new Select(driver.findElement(By.name("roomstatus")));
		              selectByValue.selectByValue("Disabled");

	}
	
	

}