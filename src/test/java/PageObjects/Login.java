package PageObjects;

//import static PageObjects.BaseObject.driver;

import java.net.MalformedURLException;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login{

	public static WebDriverWait waitVar;

	public static String baseURL = "https://www.phptravels.net/supplier";
	//public static String baseURL = "https://www.onedata.com.br/gc";
	//public WebDriver driver = BaseObject.getDriver();

	private WebDriver driver; 
	
	
	public Login(WebDriver d) {
		driver = d;
	}
	
	public void openPage() throws MalformedURLException, InterruptedException {
		
		
		waitVar = new WebDriverWait(driver, 30);
	}
	
	public void closeDriver() {
		driver.quit();
	}

	public void ishomepageDisplayed() {
		//waitVar.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Forget Password")));
		driver.findElement(By.xpath("//input[@name='email']")).isDisplayed();
	}

	
	public void insertEmail(String email) {
		driver.findElement(By.name("email")).sendKeys(email);
	}
	
	public void insertPassword(String password) {
		driver.findElement(By.name("password")).sendKeys(password);
	}
	
	public void clickButtonLogin() {
		driver.findElement(By.xpath("//*[@type='submit']")).click();
	}


	public void isloginsectionDisplayed() {
		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.className("auth-form-body")));
		waitVar.until(ExpectedConditions.presenceOfElementLocated(By.name("commit")));
	}	
	
	public void scroolToTop() {
		WebElement element = driver.findElement(By.tagName("body"));

		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].scrollIntoView();", element); 
	}
	
	/****************************** home ************************/
	
	public void clickMenuHotels() {
		driver.findElement(By.xpath("//*[@text='HOTELS']")).click();
	}
	
	public void clickSubMenuAddRoom() {
		driver.findElement(By.xpath("//*[@text='ADD ROOM']")).click();
	}

}