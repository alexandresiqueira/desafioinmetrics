package PageObjects;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ManageRooms {

	public static WebDriverWait waitVar;

	private WebDriver driver;

	public ManageRooms(WebDriver d) {
		driver = d;
	}


	

	public void verifyFirstRoomType() {
		String roomType = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div/div/div[1]/div[2]/table/tbody/tr[1]/td[3]/a")).getText();
		assertTrue(roomType.contains("Triple Rooms"));
	}
	
	public void verifyPrice() {
		String price = driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div/div/div[1]/div[2]/table/tbody/tr[1]/td[6]")).getText();
		assertTrue(price.contains("300"));
	}



}