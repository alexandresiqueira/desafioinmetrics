package StepDefinitions;

import java.net.MalformedURLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import PageObjects.BaseObject;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features", glue = "StepDefinitions", plugin = { "pretty",
		"html:target/cucumber", }, tags = { "@addNewRoom" }, monochrome = true

)
public class RunCukeTest {

	@BeforeClass
	public static void startTest() throws MalformedURLException, InterruptedException {
		// BaseObject.startPage();
	}

	@AfterClass
	public static void stopTest() {
		BaseObject.stop();
		System.out.println("Terminado");
	}

}