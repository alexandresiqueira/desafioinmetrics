package StepDefinitions;

import org.openqa.selenium.WebDriver;

import PageObjects.AddRoom;
import PageObjects.BaseObject;
import PageObjects.Home;
import PageObjects.Login;
import PageObjects.ManageRooms;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {

	WebDriver driver = BaseObject.startPage();

	private Login lp = new Login(driver);
	private Home home = new Home(driver);
	private AddRoom ar = new AddRoom(driver);
	private ManageRooms mr = new ManageRooms(driver);

	@Given("^I access site  phptavel$")
	public void i_access_site_phptavel() throws Throwable {
		System.out.println("I access site  phptavel");
		// lp.createDriver();
		lp.openPage();

		// lp.insertEmail("email");
		// Thread.sleep(10000);
		// lp.closeDriver();
		lp.ishomepageDisplayed();
	}

	@When("^I log in using usuario \"(.*?)\" and password \"(.*?)\"$")
	public void i_log_in_using_usuario_and_password(String user, String password) throws Throwable {
		System.out.println("I log in using usuario " + user + " and password " + password);
		lp.insertEmail(user);
		lp.insertPassword(password);
		lp.clickButtonLogin();
		Thread.sleep(5000);
	}

	@Then("^I see user is displayed in login screen$")
	public void i_see_user_is_displayed_in_login_screen() throws Throwable {
		System.out.println("I see user is displayed in login screen");
	}

	@Then("^I close aplicattion$")
	public void i_close_aplicattion() throws Throwable {
		// lp.closeDriver();
	}

	@Then("^I add a new room$")
	public void i_add_a_new_room() throws Throwable {
		home.clickMenuHotels();
		home.clickSubMenuAddRoom();

	}

	@Then("^I insert a general informations$")
	public void i_insert_a_general_informations() throws Throwable {
		// ar.clickTabMAmenities();

		// Thread.sleep(5000);
		ar.clickTabGeneral();
		ar.selectStatusGeneral();
		ar.selectRoomTypeGeneral();
		ar.selectHotelGeneral();
		ar.insertPriceGeneral();
		ar.insertQuantityGeneral();
		ar.insertMinimumStayGeneral();
		ar.insertMaxAdultsGeneral();
		ar.insertMaxChildrenGeneral();
		ar.insertExtraBedChergesGeneral();
		ar.insertExtraBedChergesGeneral();
	}

	@Then("^I insert a Amenities informations$")
	public void i_insert_a_Amenities_informations() throws Throwable {
		lp.scroolToTop();
		ar.clickTabMAmenities();
		ar.checkAmenitiesAmenitie();

		Thread.sleep(5000);

	}

	@Then("^I save this informations$")
	public void i_save_this_informations() throws Throwable {
		ar.clickSubmitAddRoom();
		Thread.sleep(10000);
	}

	@Then("^I verify if are saved$")
	public void i_verify_if_are_saved() throws Throwable {
		home.clickMenuHotels();
		home.clickSubMenuManageRoom();
		mr.verifyFirstRoomType();
		mr.verifyPrice();
		Thread.sleep(10000);
	}

}
