$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("LoginNoSitePhpTravels.feature");
formatter.feature({
  "line": 1,
  "name": "login on site phptravels",
  "description": "",
  "id": "login-on-site-phptravels",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 11,
  "name": "Cadastrar Nova Room",
  "description": "",
  "id": "login-on-site-phptravels;cadastrar-nova-room",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@login"
    },
    {
      "line": 10,
      "name": "@addNewRoom"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I access site  phptavel",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "I log in using usuario \"supplier@phptravels.com\" and password \"demosupplier\"",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I see user is displayed in login screen",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "I add a new room",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "I insert a general informations",
  "keyword": "Then "
});
formatter.step({
  "line": 17,
  "name": "I insert a Amenities informations",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I save this informations",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "I verify if are saved",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "I close aplicattion",
  "keyword": "Then "
});
formatter.match({
  "location": "Steps.i_access_site_phptavel()"
});
formatter.result({
  "duration": 18607217600,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "supplier@phptravels.com",
      "offset": 24
    },
    {
      "val": "demosupplier",
      "offset": 63
    }
  ],
  "location": "Steps.i_log_in_using_usuario_and_password(String,String)"
});
formatter.result({
  "duration": 5290745300,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_see_user_is_displayed_in_login_screen()"
});
formatter.result({
  "duration": 45200,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_add_a_new_room()"
});
formatter.result({
  "duration": 14545218200,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_insert_a_general_informations()"
});
formatter.result({
  "duration": 3497947700,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_insert_a_Amenities_informations()"
});
formatter.result({
  "duration": 5382981500,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_save_this_informations()"
});
formatter.result({
  "duration": 10089117500,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_verify_if_are_saved()"
});
formatter.result({
  "duration": 13459019400,
  "status": "passed"
});
formatter.match({
  "location": "Steps.i_close_aplicattion()"
});
formatter.result({
  "duration": 25700,
  "status": "passed"
});
});